package au.com.nab.api.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {
    private static final long serialVersionUID = -568062862924530939L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}