package au.com.nab.api.customer.domain;

public enum Title {
    Mr,
    Mrs,
    Ms,
    Mstr
}
