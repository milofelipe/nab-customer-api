package au.com.nab.api.customer.domain;

public enum MaritalStatus {
    Single,
    Married,
    Widowed,
    Divorced,
    Separated,
    RegisteredPartnership
}
