package au.com.nab.api.customer.domain;

public enum Gender {
    Male,
    Female
}
