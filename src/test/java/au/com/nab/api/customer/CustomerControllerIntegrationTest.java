package au.com.nab.api.customer;

import au.com.nab.api.customer.domain.Address;
import au.com.nab.api.customer.domain.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerControllerIntegrationTest {
    @Autowired
    private TestRestTemplate testRestTemplate;
    @LocalServerPort
    private int port;
    private long testCustomer1Id;
    private long testCustomer2Id;
    private long testCustomer3Id;

    private String getTestRootUrl() {
        return "http://localhost:" + port;
    }

    @Before
    public void setup() {
        // Create 3 test customers
        Customer customer = new Customer();
        customer.setFirstName("Michael");
        customer.setMiddleName("Geoffrey");
        customer.setSurname("Jones");
        ResponseEntity<Customer> customerResponse = testRestTemplate.postForEntity(getTestRootUrl() + "/api/v1/customers", customer, Customer.class);
        testCustomer1Id = customerResponse.getBody().getId();  // Get ID to be used in update and delete tests

        customer = new Customer();
        customer.setFirstName("Paul");
        customer.setMiddleName("Gustave");
        customer.setSurname("Simonon");
        customerResponse = testRestTemplate.postForEntity(getTestRootUrl() + "/api/v1/customers", customer, Customer.class);
        testCustomer2Id = customerResponse.getBody().getId();

        customer = new Customer();
        customer.setFirstName("Nicholas");
        customer.setMiddleName("Bowen");
        customer.setSurname("Headon");
        customerResponse = testRestTemplate.postForEntity(getTestRootUrl() + "/api/v1/customers", customer, Customer.class);
        testCustomer3Id = customerResponse.getBody().getId();
    }

    @After
    public void cleanup() {
        testRestTemplate.delete(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id);
        testRestTemplate.delete(getTestRootUrl() + "/api/v1/customers/" + testCustomer2Id);
        testRestTemplate.delete(getTestRootUrl() + "/api/v1/customers/" + testCustomer3Id);
    }

    @Test
    public void createCustomer() {
        Address address = new Address();
        address.setStreetNo("11");
        address.setStreetName("Nicholson St");
        address.setSuburb("Carlton");
        address.setState("VIC");

        Customer customer = new Customer();
        customer.setFirstName("John");
        customer.setMiddleName("Graham");
        customer.setSurname("Mellor");
        customer.setAddress(address);

        ResponseEntity<Customer> postResponse = testRestTemplate.postForEntity(getTestRootUrl() + "/api/v1/customers", customer, Customer.class);
        assertEquals(200, postResponse.getStatusCodeValue());

        // Cleanup
        testRestTemplate.delete(getTestRootUrl() + "/api/v1/customers/" + postResponse.getBody().getId());
    }

    @Test
    public void getAllCustomers() {
        List<Customer> customers = testRestTemplate.getForObject(getTestRootUrl() + "/api/v1/customers", List.class);
        assertEquals(3, customers.size());
    }

    @Test
    public void getCustomerById() {
        Customer customer = testRestTemplate.getForObject(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id, Customer.class);
        assertEquals("Michael", customer.getFirstName());
    }

    @Test
    public void updateCustomer() {
        Customer customer = testRestTemplate.getForObject(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id, Customer.class);
        customer.setFirstName("Mick");
        testRestTemplate.put(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id, customer);
        Customer updatedCustomer = testRestTemplate.getForObject(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id, Customer.class);
        assertEquals("Mick", updatedCustomer.getFirstName());
    }

    @Test
    public void deleteCustomer() {
        Customer customer = testRestTemplate.getForObject(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id, Customer.class);
        assertNotNull(customer);
        testRestTemplate.delete(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id);
        customer = testRestTemplate.getForObject(getTestRootUrl() + "/api/v1/customers/" + testCustomer1Id, Customer.class);
        assertEquals(0, customer.getId());
    }

    @Test
    public void getCustomerById_ResourceNotFound() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = testRestTemplate.exchange(getTestRootUrl() + "/api/v1/customers/100",
                HttpMethod.GET, entity, String.class);
        assertEquals(404, response.getStatusCodeValue());
        assertTrue(response.getBody().contains("Customer with ID '100' is not found."));
    }

    @Test
    public void createCustomer_invalidCreditRating() {
        Customer customer = new Customer();
        customer.setFirstName("John");
        customer.setMiddleName("Graham");
        customer.setSurname("Mellor");
        customer.setCreditRating(105);

        ResponseEntity<Customer> postResponse = testRestTemplate.postForEntity(getTestRootUrl() + "/api/v1/customers", customer, Customer.class);
        assertEquals(500, postResponse.getStatusCodeValue());
    }
}