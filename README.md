**This ia a fictional Customer Rest API implemented using Spring Boot**

========================================

To build JAR file:

mvn package

To run:

java -jar target/customer-api-0.0.1-SNAPSHOT.jar

*Uses Java 8, Spring Boot, Maven

========================================

API endpoint is **/api/v1/customers**.

## POST

To create a new customer, submit a POST request to /api/v1/customers with HTTP header Content-Type "application/json". 
JSON to be submitted should be in this format:

```
{
    "firstName": "John",
    "middleName": "Doe",
    "surname": "Smith",
    "initials": "JDS",
    "title": "Mr",
    "address": {
    	"streetNo": "1",
    	"streetName": "First Street",
    	"suburb": "Williams Landing",
    	"city": "Wyndham",
    	"state": "VIC",
    	"country": "Australia",
    	"pinCode": "3000"
    },
    "gender": "Male",
    "maritalStatus": "Single",
    "creditRating": 89,
    "nabCustomer": true
}
```

**Required fields**
- firstName
- middleName
- surname

**Valid values for the following fields**

Title: Mr, Mrs, Ms, and Mstr

Marital Status: Single, Married, Widowed, Divorced, Separated, and RegisteredPartnership

Gender: Male, Female

Credit Rating: 0 to 100

## PUT

To update a customer, submit a PUT request to /api/v1/customers/{id} with HTTP header Content-Type "application/json". Replace {id} with the 
"id" of the customer record to be updated. JSON to be submitted should be in the same format as the POST request.

## DELETE

To delete a customer, submit a DELETE request to /api/v1/customers/{id}. Replace {id} with the 
"id" of the customer record to be delete. If delete is successful, JSON response will be:

```
{
    "deleted": true
}
```

If customer record does not exist, response will be:

```
{
    "timestamp": "2019-01-07T06:58:50.246+0000",
    "message": "Customer with ID '1023' is not found.",
    "details": "uri=/api/v1/customers/1023"
}
```

## GET

To get a specific customer record, submit a GET request to /api/v1/customers/{id}. Replace {id} with the 
"id" of the customer record to be retrieved.

## GET ALL

To get all customer records, submit a GET request to /api/v1/customers.


